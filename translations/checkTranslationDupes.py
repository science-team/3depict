#!/usr/bin/python

import sys
import re


def main():

	if(len(sys.argv) != 2):
		print("not enough arguments")
		return

	filename=sys.argv[1]
	f = open(filename)

	if not f:
		sys.exit("Unable to open file"  +str(sys.argv[1]) +". Exiting")

	curLine=0

	THRESHOLD=6
	while True: 
		curLine=curLine+1;
		s=f.readline();

		if(not s) : 
			break;


		if re.match("msgid.*",s):
			msgid=s[6:];
		elif re.match("msgstr.*",s):
			msgstr=s[7:];

			if(len(msgstr) < THRESHOLD):
				continue

			if(msgid == msgstr and re.match(".*[A-z].*",msgid)):
					print "Duplicate id at "  + str(curLine) + " value is :" + msgstr


if __name__ == "__main__" : 
	main()
