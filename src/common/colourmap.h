/*
 *	colourmap.h - Colour continumms definitions
 *	Copyright (C) 2018, D Haley 

 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.

 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.

 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _COLORMAP_H_
#define _COLORMAP_H_

#include <string>
#include <vector>

#include "common/basics.h"

//List of the available colourmaps
enum
{
	COLOURMAP_JET,
	COLOURMAP_HOT,
	COLOURMAP_COLD,
	COLOURMAP_GRAY,
	COLOURMAP_CYCLIC,
	COLOURMAP_GENERAL,
	COLOURMAP_BLUE,
	COLOURMAP_RAND,
	COLOURMAP_INFERNO,
	COLOURMAP_VIRIDIS,
	COLOURMAP_ENUM_END
};

//FIXME: Better location for this enum
enum
{
	COLOURMAP_MODE_NONE,
	COLOURMAP_MODE_BUILTIN,
	COLOURMAP_MODE_CUSTOM,
	COLOURMAP_MODE_ENUM_END
};

//!get colour for specific map
//returns char in 0->255 range 
void colourMapWrap(unsigned int mapID,unsigned char *rgb, 
		float value, float min,float max,bool reverse);

std::string getColourMapName(unsigned int mapID);

//!Generate the colour used in a custom colour map (linear interpolated). Input array must be 3 bytes long
// colour map must have nonzero number of entries. Input vector must be sorted by first element of pair.
// input pair should be from 0->1 inidicating the position of the gradient in minV/maxV scaling.
void applyCustomColourMap(const std::vector<std::pair<float,ColourRGBAf> > &cMap, unsigned char *rgbfThis, 
							unsigned char &alphaThis, float value, float minV, float maxV);

#ifdef DEBUG
//Run the colour map tests
bool testColourMap();
#endif

#endif

