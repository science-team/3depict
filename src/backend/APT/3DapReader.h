/* 
 * 3DapReader.h- 3DAP (3D Atom Probe)  data file format parser/reconstructor
 * Copyright (C) 2018, D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef THREEDAPREADER_H
#define THREEDAPREADER_H

#include <vector>

#include "ionhit.h"
#include "common/stringFuncs.h"

enum
{
	OPSREADER_FORMAT_CLINE_ERR=1,
	OPSREADER_FORMAT_DETECTORLINE_ERR,
	OPSREADER_FORMAT_LINE_DASH_ERR,
	OPSREADER_FORMAT_LINE_VOLTAGE_ERR,
	OPSREADER_FORMAT_LINE_VOLTAGE_NOBETA,
	OPSREADER_FORMAT_LINE_VOLTAGE_DATA_ERR,
	OPSREADER_FORMAT_LINE_VOLTAGE_DATACOUNT_ERR,
	OPSREADER_FORMAT_LINETYPE_ERR,
	OPSREADER_FORMAT_CHANNELS_ERR,
	OPSREADER_CHANNELS_DATA_ERR,
	OPSREADER_FORMAT_SLINE_EVENTCOUNT_ERR,
	OPSREADER_FORMAT_SLINE_EVENTDATA_ERR,
	OPSREADER_FORMAT_SLINE_FORMAT_ERR,
	OPSREADER_FORMAT_SLINE_PREFIX_ERR,
	OPSREADER_FORMAT_DUPLICATE_SYSDATA,
	OPSREADER_FORMAT_DUPLICATE_DETECTORSIZE,
	OPSREADER_FORMAT_TRAILING_DASH_ERR,
	OPSREADER_FORMAT_DOUBLEDASH,
	OPSREADER_OPEN_ERR,
	OPSREADER_READ_ERR,
	OPSREADER_ABORT_ERR,
	OPSREADER_ENUM_END
};

extern const char *OPS_ENUM_ERRSTRINGS[]; 

struct THREEDAP_RECON_PARAMS
{
	//Do we want to use TOF correction?
	bool useTOFDistortion;
	// Minimum and maximum m/c values to use
	// when performing TOF correction
	float minMtoCDistort, maxMtoCDistort;
	//radius of cylinder to use for reconstruction
	float radiusCylinder;

	//Detection efficiency
	float detectionEfficiency;
	//volume of ion to use
	float ionVolume;
};

//!Reconstruct an "OPS" formatted file (from a 3Dap) to an IonHit vector
unsigned int reconstruct3DapData(const char *file, 
	const THREEDAP_RECON_PARAMS &params, 
	unsigned int &progress, std::atomic<bool> &wantAbort,
	std::vector<IonHit> &ionData);

#endif
