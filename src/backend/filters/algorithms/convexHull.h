/* 
 * convexHull.h - Convex hull implementation wrapper
 * Copyright (C) 2018, D Haley
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONVEXHULL_H 
#define CONVEXHULL_H

#include "../../filter.h"

//FIXME: I should not need to include this, I do this to get access to the RDF_ error codes
#include "spatial.h"
#include "config.h"


//QHull library
//Build fix for qhull ; wx defines powerpc without
//assigning a value, causing build fail on powerpc
#ifdef __POWERPC__
	#pragma push_macro("__POWERPC__")
	// Defining to 1 also causes build to fail on check:
	// #if __MWERKS__ && __POWERPC__
	// Undefine instead, to restore us to the state that
	// GCC is in before wxWidgets messes things up, since
	// that's the state Qhull should expect.
	#undef __POWERPC__
#endif
extern "C"
{
	#include <qhull/qhull_a.h>
}
#ifdef __POWERPC__
	#pragma pop_macro("__POWERPC__")
#endif

enum
{
	HULL_ERR_NO_MEM=1,
	HULL_ERR_USER_ABORT,
	HULL_ERR_QHULL_NOINIT,
	HULL_ERR_ENUM_END
};

//Compute the convex hull of a set of input points from fiilterstream data
unsigned int computeConvexHull(const std::vector<const FilterStreamData*> &data, 
			unsigned int *progress, 
			std::vector<Point3D> &hullPts, bool wantVolume, bool freeHull=true);
//Compute the convex hull of a set of input points
unsigned int computeConvexHull(const std::vector<Point3D> &data, 
			unsigned int *progress, const bool &abortPtr,
			std::vector<Point3D> &hullPts, bool wantVolume, bool freeHull=true);

//Release the memory held by qhull, and notify the computeConvexHull routines that this has been done
void freeConvexHull();
#else
struct HULLTRI
{
	unsigned int vertex[3];
};

//Compute the convex hull of a set of input points from fiilterstream data
unsigned int computeConvexHull(const std::vector<const FilterStreamData*> &data, 
			unsigned int *progress, 
			std::vector<Point3D> &hullPts, std::vector<HULLTRI> &facets);
//Compute the convex hull of a set of input points
unsigned int computeConvexHull(const std::vector<Point3D> &data, 
			unsigned int *progress, const bool &abortPtr,
			std::vector<Point3D> &hullPts, std::vector<HULLTRI> &facets);

float getConvexVolume(const vector<Point3D> &pts, 
			const vector<HULLTRI> &facets);
#endif

#ifdef DEBUG
bool testHull();
#endif
