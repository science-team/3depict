\contentsline {section}{\numberline {1}Foreword}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Introduction}{1}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Background}{1}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}What is Open Source?}{1}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Requirements}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Platform specific notes}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Getting help}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Who wrote this program?}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Alternate documentation}{3}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Helping out}{3}{subsection.1.7}
\contentsline {section}{\numberline {2}Basics}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Getting started}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Licence}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Installing the program}{4}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Understanding the interface}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}The Filter Tree}{5}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Properties}{5}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}The 3D View}{6}{subsubsection.2.2.3}
\contentsline {paragraph}{Basic movement}{7}{figure.4}
\contentsline {subsubsection}{\numberline {2.2.4}Plot area}{7}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Console}{7}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Tools panel}{7}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Usage fundamentals}{9}{subsection.2.3}
\contentsline {section}{\numberline {3}Quick start}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Loading data}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Loading an analysis}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Ranging}{11}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Spectrum}{12}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Composition profiles}{12}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Counting Points and measuring volume}{12}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Concentration surface and slices}{13}{subsection.3.7}
\contentsline {section}{\numberline {4}Understanding the program}{15}{section.4}
\contentsline {subsection}{\numberline {4.1}Filters}{15}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Trees}{15}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Stashes}{16}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Plots}{17}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Cameras}{17}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Effects}{18}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Program actions}{18}{subsection.4.7}
\contentsline {subsubsection}{\numberline {4.7.1}Save}{18}{subsubsection.4.7.1}
\contentsline {subsubsection}{\numberline {4.7.2}Undo}{18}{subsubsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.3}Raw Data}{18}{subsubsection.4.7.3}
\contentsline {subsubsection}{\numberline {4.7.4}Export Menu}{18}{subsubsection.4.7.4}
\contentsline {subsubsection}{\numberline {4.7.5}Ranging dialog}{19}{subsubsection.4.7.5}
\contentsline {subsubsection}{\numberline {4.7.6}Autosave}{21}{subsubsection.4.7.6}
\contentsline {subsubsection}{\numberline {4.7.7}Export Animation}{21}{subsubsection.4.7.7}
\contentsline {section}{\numberline {5}Detailed Reference}{25}{section.5}
\contentsline {subsection}{\numberline {5.1}Data types}{25}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Ions}{25}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Plots}{25}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Range}{25}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Voxels}{25}{subsubsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.5}Drawables}{25}{subsubsection.5.1.5}
\contentsline {subsection}{\numberline {5.2}Filters}{25}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Data load}{26}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Voxel Load}{27}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Downsampling}{27}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}Ion Information}{27}{subsubsection.5.2.4}
\contentsline {subsubsection}{\numberline {5.2.5}Ranging}{28}{subsubsection.5.2.5}
\contentsline {subsubsection}{\numberline {5.2.6}Bounding Box}{29}{subsubsection.5.2.6}
\contentsline {subsubsection}{\numberline {5.2.7}Clipping}{29}{subsubsection.5.2.7}
\contentsline {subsubsection}{\numberline {5.2.8}Spectrum}{30}{subsubsection.5.2.8}
\contentsline {subsubsection}{\numberline {5.2.9}Profile}{31}{subsubsection.5.2.9}
\contentsline {subsubsection}{\numberline {5.2.10}Spatial Analysis}{31}{subsubsection.5.2.10}
\contentsline {paragraph}{Algorithms}{31}{section*.3}
\contentsline {paragraph}{Radial Distribution:}{32}{section*.4}
\contentsline {paragraph}{Axial Distribution:}{32}{section*.5}
\contentsline {paragraph}{Binomial:}{32}{section*.6}
\contentsline {paragraph}{Point em/re-placement :}{32}{section*.7}
\contentsline {paragraph}{Local Chemistry:}{32}{section*.8}
\contentsline {subsubsection}{\numberline {5.2.11}Concentration Filtering : }{33}{subsubsection.5.2.11}
\contentsline {subsubsection}{\numberline {5.2.12}Clustering analysis}{33}{subsubsection.5.2.12}
\contentsline {subsubsection}{\numberline {5.2.13}External Program}{35}{subsubsection.5.2.13}
\contentsline {paragraph}{Command syntax: }{35}{section*.9}
\contentsline {paragraph}{Prior to program execution:}{35}{section*.10}
\contentsline {paragraph}{After program execution:}{35}{section*.11}
\contentsline {subsubsection}{\numberline {5.2.14}Annotation}{36}{subsubsection.5.2.14}
\contentsline {paragraph}{Text}{36}{section*.12}
\contentsline {paragraph}{Arrow, Arrow with Text}{36}{section*.13}
\contentsline {paragraph}{Angle Measurement}{36}{section*.14}
\contentsline {subsubsection}{\numberline {5.2.15}Voxels}{37}{subsubsection.5.2.15}
\contentsline {subsubsection}{\numberline {5.2.16}Ion Colour}{39}{subsubsection.5.2.16}
\contentsline {subsubsection}{\numberline {5.2.17}Ion Transform}{39}{subsubsection.5.2.17}
\contentsline {section}{\numberline {6}Attributions}{40}{section.6}
\contentsline {section}{\numberline {7}Licence}{40}{section.7}
\contentsline {section}{\numberline {8}Appendices}{40}{section.8}
\contentsline {subsection}{\numberline {8.1}Paths}{40}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}File formats}{40}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}State file}{40}{subsubsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.2}Remap files}{41}{subsubsection.8.2.2}
\contentsline {subsubsection}{\numberline {8.2.3}Range files}{42}{subsubsection.8.2.3}
\contentsline {subsubsection}{\numberline {8.2.4}POS files}{43}{subsubsection.8.2.4}
\contentsline {subsubsection}{\numberline {8.2.5}Text files}{43}{subsubsection.8.2.5}
\contentsline {subsubsection}{\numberline {8.2.6}Voxel data}{44}{subsubsection.8.2.6}
\contentsline {subsection}{\numberline {8.3}External Program Examples}{44}{subsection.8.3}
\contentsline {subsubsection}{\numberline {8.3.1}Scilab}{44}{subsubsection.8.3.1}
\contentsline {subsubsection}{\numberline {8.3.2}Python}{48}{subsubsection.8.3.2}
\contentsline {subsubsection}{\numberline {8.3.3}Bash}{50}{subsubsection.8.3.3}
\contentsline {subsubsection}{\numberline {8.3.4}C/C++}{52}{subsubsection.8.3.4}
\contentsline {subsection}{\numberline {8.4}Modifying the program}{56}{subsection.8.4}
\contentsline {subsubsection}{\numberline {8.4.1}Development tools}{56}{subsubsection.8.4.1}
\contentsline {subsubsection}{\numberline {8.4.2}Getting yourself set up}{57}{subsubsection.8.4.2}
\contentsline {subsubsection}{\numberline {8.4.3}Changing stuff}{57}{subsubsection.8.4.3}
